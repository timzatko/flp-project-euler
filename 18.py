# https://projecteuler.net/problem=18

from functools import lru_cache, reduce
from itertools import islice


def triangle():
    return ((map(int, line.rstrip('\n').split(' '))) for line in open('18.txt'))


def size():
    return reduce(lambda a, _: a + 1, triangle(), 0)


def in_triangle(a, b):
    return b <= a < size()


def triangle_at(row, col):
    return next(islice(next(islice(triangle(), row, row + 1)), col, col + 1))


@lru_cache(maxsize=4096)
def val(row, col):
    if not in_triangle(row, col):
        return 0
    return triangle_at(row, col) + max(val(row + 1, col), val(row + 1, col + 1))


def solve():
    return val(0, 0)


print(solve())
# 1074
