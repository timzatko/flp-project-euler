# https://projecteuler.net/problem=4

from functools import reduce


def get_min(n):
    return 10 ** (n - 1)


def get_max(n):
    return 10 ** n


def largest_palindrome(n):
    return reduce(lambda a, b: max(a, b), [a * b for a in reversed(range(get_min(n), get_max(n))) for b in reversed(range(get_min(n), get_max(n))) if str(a * b) == str(a * b)[::-1]], 0)


print(largest_palindrome(3))
# 906609
