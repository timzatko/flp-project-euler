# https://projecteuler.net/problem=12

from math import sqrt
from itertools import dropwhile


def triangle_numbers():
    n, i = 0, 1
    while True:
        n += i
        i += 1
        yield n


def divisors(n):
    return sum((1 if i ** 2 is n else 2 for i in range(1, int(sqrt(n) + 1)) if not n % i))


def solve(n):
    return next(dropwhile(lambda a: a[0] < n, map(lambda a: (divisors(a), a), triangle_numbers())))[1]


print(solve(500))
# 76576500
