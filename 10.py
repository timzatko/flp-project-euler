# https://projecteuler.net/problem=9

from functools import reduce
import sys

sys.setrecursionlimit(100000)


def util_recursive(sieve, start, end, step):
    if start < end:
        sieve[start] = False
        util_recursive(sieve, start + step, end, step)
    return sieve


def sieve_of_eratosthenes_recursive(sieve, start, end):
    if start < end:
        if sieve[start]:
            sieve_of_eratosthenes_recursive(util_recursive(sieve, start * 2, end, start), start + 1, end)
    return sieve


def sum_of_n_primes(n):
    return reduce(lambda a, b: a + b[0] if b[1] else a, enumerate(sieve_of_eratosthenes_recursive(list(map(lambda a: a % 2 or a % 3 or a % 5 or a % 7 if a >= 2 else False, range(0, n + 1))), 2, n + 1)), 0)


print(sum_of_n_primes(2000000))
# 142913828922
