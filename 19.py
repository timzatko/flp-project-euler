# https://projecteuler.net/problem=19

from datetime import date
from functools import reduce


def solve(from_, to_):
    return reduce(lambda a, b: a + b, (1 for year in range(from_ + 1, to_ + 1) for month in range(1, 13) if date(year, month, 1).weekday() == 6), 0)


print(solve(1900, 2000))
# 171
