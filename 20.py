# https://projecteuler.net/problem=20

from math import factorial


def solve(n):
    return sum(map(int, str(factorial(n))))


print(solve(100))
# 648
