# https://projecteuler.net/problem=35

from itertools import dropwhile
from functools import reduce
from math import sqrt


def circular(n):
    string = str(n)
    for k in range(0, len(string)):
        yield int(string[k:] + string[:k])


def is_prime(i):
    return reduce(lambda a, b: a if i % b else False, range(2, int(sqrt(i)) + 1), True)


def is_circular(n):
    return next(dropwhile(lambda a: is_prime(a), circular(n)), None) is None


def solve(n):
    return reduce(lambda a, b: a + 1 if is_circular(b) else a, range(2, n), 0)


# Trva to trochu dlshie lebo som nepouzil eratosthenovo sito,
# vyriesil som ho uz v inej ulohe ale niesom si isty ci je dobre funkcionalne preto som ho radsej nepouzil
print(solve(1000000))
# 55
