# https://projecteuler.net/problem=48


def solve():
    return sum(map(lambda a:  (a ** a) % 10 ** 10, range(1, 1000))) % 10 ** 10


print(solve())
# 9110846700
