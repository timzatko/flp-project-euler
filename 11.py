# https://projecteuler.net/problem=11

from functools import reduce
from operator import mul


def generator(n):
    g = [list(map(int, line.rstrip('\n').split(' '))) for line in open('11.txt')]
    size = len(g)
    for i in range(0, size - n):
        for j in range(0, size - n):
            yield [g[i][j + k] for k in range(n)]
            yield [g[i + k][j] for k in range(n)]
    for i in range(n - 1, size - n):
        for j in range(n - 1, size - n):
            yield [g[i + k][j + k] for k in range(n)]
            yield [g[i - k][j + k] for k in range(n)]


def largest_product(n):
    return reduce(lambda a, b: max(a, reduce(mul, b, 1)), generator(n), 0)


print(largest_product(4))
# 70600674

