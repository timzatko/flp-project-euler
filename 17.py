# https://projecteuler.net/problem=17

from functools import reduce


numbers = {
    1000: 'thousand',
    100: 'hundred',
    90: 'ninety',
    80: 'eighty',
    70: 'seventy',
    60: 'sixty',
    50: 'fifty',
    40: 'forty',
    30: 'thirty',
    20: 'twenty',
    19: 'nineteen',
    18: 'eighteen',
    17: 'seventeen',
    16: 'sixteen',
    15: 'fifteen',
    14: 'fourteen',
    13: 'thirteen',
    12: 'twelve',
    11: 'eleven',
    10: 'ten',
    9: 'nine',
    8: 'eight',
    7: 'seven',
    6: 'six',
    5: 'five',
    4: 'four',
    3: 'three',
    2: 'two',
    1: 'one',
}


def is_thousand(n):
    return n >= 1000


def is_hundred(n):
    return n >= 100


def num_generator(n):
    while n:
        if is_thousand(n):
            yield numbers[n // 1000]
            yield numbers[1000]
            n %= 1000
        elif is_hundred(n):
            yield numbers[n // 100]
            yield numbers[100]
            n %= 100
            if n:
                yield 'and'
        else:
            for key, value in numbers.items():
                if n // key:
                    n %= key
                    yield value
                    break


def num(n):
    return reduce(lambda a, b: a + len(b), num_generator(n), 0)


def solve():
    return sum(num(n) for n in range(1, 1001))


print(solve())
# 21124
