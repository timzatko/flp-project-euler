# https://projecteuler.net/problem=9


def solve(n):
    return next(a * b * (n - a - b) for a in range(1, int(n / 3)) for b in range(a + 1, int(n / 2)) if (a ** 2) + (b ** 2) == (n - a - b) ** 2)


print(solve(1000))
# 31875000
