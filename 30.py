# https://projecteuler.net/problem=30

from functools import reduce


def digits_sum(n, e):
    return reduce(lambda a, b: a + int(b) ** e, str(n), 0)


def solve(e):
    return sum(x for x in range(2, 9 ** e * 9) if x == digits_sum(x, e))


print(solve(5))
# 443839
