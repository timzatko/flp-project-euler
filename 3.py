# https://projecteuler.net/problem=3

from functools import reduce
from math import sqrt


def find_largest_prime_factor(n):
    return reduce(lambda a, b: max(a, b), filter(lambda x: not n % x and reduce(lambda a, b: False if not x % b else a, range(3, int(sqrt(x)) + 1), True), range(1, int(sqrt(n)))), 0)


print(find_largest_prime_factor(600851475143))
# 6857
