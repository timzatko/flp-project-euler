# https://projecteuler.net/problem=14

from functools import lru_cache, reduce
import sys

sys.setrecursionlimit(100000)


@lru_cache(maxsize=4096)
def sequence(n):
    if n == 1:
        return 1
    elif n % 2:
        return sequence(3 * n + 1) + 1
    else:
        return sequence(n // 2) + 1


def the_longest_sequence(n):
    return reduce(lambda a, b: a if a[1] > b[1] else b, ((x, sequence(x)) for x in range(1, n)), (0, 0))[0]


print(the_longest_sequence(1000000))
# 837799
