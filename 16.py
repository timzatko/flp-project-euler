# https://projecteuler.net/problem=16


def solve(n):
    return sum(map(int, str(2 ** n)))


print(solve(1000))
# 1366
