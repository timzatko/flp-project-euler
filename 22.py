# https://projecteuler.net/problem=18

from functools import reduce


def names():
    return sorted(map(lambda name: str.replace(name, '"', ''), str.split(reduce(lambda lines, line: lines + line, open('22.txt').readlines()), ',')))


def solve():
    return sum(map(lambda a: (a[0] + 1) * a[1], enumerate(map(lambda name: sum(map(lambda char: ord(char) - ord('A') + 1, name)), names()))))


print(solve())
# 871198282
