# https://projecteuler.net/problem=6


def find(n):
    return sum(range(1, n + 1)) ** 2 - sum(map(lambda x: x ** 2, range(1, n + 1)))


print(find(100))
# 25164150
