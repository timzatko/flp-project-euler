# https://projecteuler.net/problem=5

from functools import reduce
from itertools import dropwhile


def generator():
    x = 0
    while True:
        x += 2520
        yield x


def evenly_divisible():
    return next(dropwhile(lambda x: not reduce(lambda a, b: False if x % b else a, range(11, 21), True), generator()))


print(evenly_divisible())
# 232792560
