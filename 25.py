# https://projecteuler.net/problem=25

from itertools import dropwhile


def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


def solve():
    return next(dropwhile(lambda a: len(str(a[1])) < 1000, enumerate(fib())))[0]


print(solve())
# 4782
