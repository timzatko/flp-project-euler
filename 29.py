# https://projecteuler.net/problem=29


def solve():
    return len(set(a ** b for a in range(2, 101) for b in range(2, 101)))


print(solve())
# 9183
