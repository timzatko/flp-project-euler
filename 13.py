# https://projecteuler.net/problem=13

from functools import reduce


def first_ten(numbers):
    return int(str(reduce(lambda a, b: a + b, map(lambda n: int(n[:20]), numbers), 0))[:10])


print(first_ten((line.rstrip('\n') for line in open('13.txt'))))
# 5537376230
