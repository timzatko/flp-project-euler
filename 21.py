# https://projecteuler.net/problem=21


def d(n):
    return sum(x for x in range(1, int(n / 2) + 1) if not n % x)


def solve(n):
    return sum(x for x in range(4, n) if d(d(x)) == x and d(x) != x)


print(solve(10000))
# 31626
