# https://projecteuler.net/problem=1

from functools import reduce


def find_sum(n):
    return reduce(lambda a, b: a + b, (x for x in range(1, n) if not x % 3 or not x % 5), 0)


print(find_sum(1000))
# 233168
