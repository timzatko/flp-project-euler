# https://projecteuler.net/problem=15

from math import factorial


def lattice_paths(n):
    return int(factorial(2 * n) / (factorial(n) ** 2))


print(lattice_paths(20))
# 137846528820
