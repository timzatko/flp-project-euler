# https://projecteuler.net/problem=24

from itertools import permutations, islice


def solve():
    return next(islice((''.join(x) for x in permutations("0123456789")), 999999, 1000000))


print(solve())
# 2783915460
