# https://projecteuler.net/problem=7

from itertools import islice
from functools import reduce
from math import sqrt


def primes():
    i = 2
    while True:
        i += 1
        if reduce(lambda a, b: a if i % b else False, range(2, int(sqrt(i)) + 1), True):
            yield i


def kth_prime(n):
    return next(islice(primes(), n - 2, n - 1))


print(kth_prime(10001))
# 104743
