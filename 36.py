# https://projecteuler.net/problem=36


def is_palindromic(n):
    return n == n[::-1]


def is_bin_dec_palindromic(n):
    return is_palindromic(str(n)) and is_palindromic("{0:b}".format(n))


def solve():
    return sum(filter(is_bin_dec_palindromic, range(0, 1000000)))


print(solve())
# 872187
