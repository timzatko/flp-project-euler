# https://projecteuler.net/problem=2

from functools import reduce


def fib(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a + b


def find_sum(n):
    return reduce(lambda a, b: a + b, filter(lambda a: not a % 2, fib(n)), 0)


print(find_sum(4000000))
# 4613732
